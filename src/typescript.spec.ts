import { expect } from 'chai';
import { extractErrorMessage } from './typescript';

describe('typescript', function () {
  describe('extractErrorMessage', function () {
    it('Error Object', () => {
      const msg = extractErrorMessage(new Error('test'));
      expect(msg).to.eq('test');
    });

    it('string', () => {
      const msg = extractErrorMessage('test');
      expect(msg).to.eq('test');
    });

    it('object', () => {
      expect(extractErrorMessage({ name: 'abc' })).to.eq('');
      expect(extractErrorMessage({ message: 'abc' })).to.eq('abc');
      expect(extractErrorMessage({ msg: 'okok' })).to.eq('okok');
    });
  });
});
