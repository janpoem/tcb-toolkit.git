import { assert as _ } from 'chai';
import { setupI18n, i18n, TcbToolkitI18nBasic, i18nAll } from './i18n';

describe('i18n', function () {
  const oldText = i18nAll();

  const newText: TcbToolkitI18nBasic = {
    unknownError: 'ue',
    validationError: 've',
    notObject: 'no',
  };

  it('setup new text', function () {
    setupI18n(newText);

    _.equal(i18n('unknownError'), newText.unknownError);
    _.equal(i18n('validationError'), newText.validationError);
    _.equal(i18n('notObject'), newText.notObject);
  });

  it('setup old text', function () {
    setupI18n(oldText);

    _.equal(i18n('unknownError'), oldText.unknownError);
    _.equal(i18n('validationError'), oldText.validationError);
    _.equal(i18n('notObject'), oldText.notObject);
  });
});
