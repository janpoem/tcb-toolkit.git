/**
 * 判断并推断变量类型是否为字符串类型
 *
 * @param v
 */
export const isString = (v: unknown): v is string => v != null && typeof v === 'string';

/**
 * 是否为空字符串
 *
 * @param v
 */
export const isEmptyString = (v: unknown): boolean => !isString(v) || v.trim() === '';
