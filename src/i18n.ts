export type TcbToolkitI18nBasic = {
  unknownError: string;
  notObject: string;
  validationError: string;
} & {
  [key: string]: string;
};

const globalI18nText: TcbToolkitI18nBasic = {
  unknownError: '未知错误！',
  notObject: `不是一个对象结构！`,
  validationError: '对象字段检验失败！',
};

export const setupI18n = (text: Partial<TcbToolkitI18nBasic>) => {
  Object.assign(globalI18nText, text);
};

export const i18nAll = (): TcbToolkitI18nBasic => Object.assign({}, globalI18nText);

export const i18n = <T extends TcbToolkitI18nBasic = TcbToolkitI18nBasic>(key: keyof T): string | undefined => {
  // @ts-ignore globalI18nText[key]
  return globalI18nText[key] ?? undefined;
};
