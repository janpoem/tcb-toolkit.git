import { expect } from 'chai';
import { isRecord, ValidationError, withFields } from './object';

type AType = {
  a: string,
  b: string,
};

interface AInterface {
  a: string,
  b: string,
  key: string,
  id: number,
}

describe('typescript', function () {
  describe('isRecord', function () {
    it('check record type', () => {
      expect(isRecord({ a: 'a' })).to.eq(true);
      expect(isRecord(['a', 'b'])).to.eq(false);
      expect(isRecord(() => 'ok')).to.eq(false);
    });

    it('infer type', () => {
      const obj = { a: 'a' };
      if (isRecord(obj)) {
        // infer type
        const { a } = obj;
        expect(a).to.eq('a');
      }
    });

    it('infer type', () => {
      expect(isRecord<AType>({ k: 12 })).to.eq(true);
      expect(isRecord<AInterface>({ k: 12 })).to.eq(true);
    });
  });

  describe('withFields', function () {
    it('withFields array #1', () => {
      const obj = { name: 'string', id: 'ok' };
      expect(withFields(obj, ['name', 'id'])).to.eq(true);
    });

    it('withFields array #2', () => {
      const obj = { name: 'string' };
      const v = ['id', 'name'];
      expect(() => withFields(obj, v)).to.throws(ValidationError);
    });

    it('withFields object string', () => {
      const obj = { name: 'string', id: 'ok' };
      expect(
        withFields(obj, {
          name: 'string',
        }),
      ).to.eq(true);
    });

    it('withFields object string#error', () => {
      const obj = { name: 123 };
      expect(() =>
        withFields(obj, {
          name: 'string',
        }),
      ).to.throws(ValidationError);
    });

    it('withFields object number', () => {
      const obj = { name: 123 };
      expect(
        withFields(obj, {
          name: 'number',
        }),
      ).to.eq(true);
    });

    it('withFields object number#error', () => {
      const obj = { name: 'abc' };
      expect(() =>
        withFields(obj, {
          name: 'number',
        }),
      ).to.throws(ValidationError);
    });

    it('withFields object boolean', () => {
      const obj = { name: false };
      expect(
        withFields(obj, {
          name: 'boolean',
        }),
      ).to.eq(true);
    });

    it('withFields object boolean#error', () => {
      const obj = { name: 'abc' };
      expect(() =>
        withFields(obj, {
          name: 'boolean',
        }),
      ).to.throws(ValidationError);
    });

    it('withFields object callback#1', () => {
      const obj = { name: 'abc' };
      expect(
        withFields(obj, {
          name: () => true,
        }),
      ).to.eq(true);
    });

    it('withFields object callback#2', () => {
      const obj = { name: 'abc' };
      expect(() =>
        withFields(obj, {
          name: () => false,
        }),
      ).to.throws(ValidationError);
    });

    it('withFields object callback#3', () => {
      const obj = { name: 'abc' };
      expect(() =>
        withFields(obj, {
          name: () => {
            throw new Error('okok');
          },
        }),
      ).to.throws(ValidationError);
    });


  });


});
