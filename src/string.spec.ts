import { expect } from 'chai';
import { isString, isEmptyString } from './string';

describe('typescript', function () {
  describe('isString', function () {
    it('check string type', () => {
      expect(isString({ a: 'a' })).to.eq(false);
      expect(isString(['a', 'b'])).to.eq(false);
      expect(isString(() => 'ok')).to.eq(false);
      expect(isString('')).to.eq(true);
      expect(isString(1)).to.eq(false);
      expect(isString(Symbol('xxx'))).to.eq(false);
    });
  });

  describe('notEmptyString', function () {
    it('isEmptyString', () => {
      expect(isEmptyString({ a: 'a' })).to.eq(true);
      expect(isEmptyString(['a', 'b'])).to.eq(true);
      expect(isEmptyString(() => 'ok')).to.eq(true);
      expect(isEmptyString('')).to.eq(true);
      expect(isEmptyString('\n')).to.eq(true);
      expect(isEmptyString('  ')).to.eq(true);
      expect(isEmptyString(`\t`)).to.eq(true);
      expect(isEmptyString(` asd `)).to.eq(false);
      expect(isEmptyString(Symbol('xxx'))).to.eq(true);
    });
  });
});
