# tcb-toolkit

腾讯云 cloudbase（tcb） 工具箱，主要针对在 Typescript 环境下的 tcb 开发。

基于 `@cloudbase/node-sdk` `2.8.1` 参考 [NPM versions](https://www.npmjs.com/package/@cloudbase/node-sdk/v/2.9.0?activeTab=versions)，该版本已经发布超过 1年以上。

近期 tcb 大量更新，node.js 版本也支持到 `16.13`，已经是非常成熟，可用度很高，非常推荐使用。

## TCB 简明教程

TCB 全称 **Tencent Cloud Base** 。

重要的文档入口（腾讯云官方那个文档内容太少）：

- [@cloudbase/cli 文档入口](https://docs.cloudbase.net/cli-v1/intro)
- [云函数文档入口](https://docs.cloudbase.net/cloud-function/introduce)

目前 CloudBase 支持最新的 Node.js 版本为 16.13。

简写说明：

- tcb - [@cloudbase/cli](https://www.npmjs.com/package/@cloudbase/cli) ，全局 tcb 指令（升级版），需要 `npm i @cloudbase/cli -g`
- node-sdk - [@cloudbase/node-sdk](https://www.npmjs.com/package/@cloudbase/node-sdk) ， node.js 后端 SDK
- js-sdk - [@cloudbase/js-sdk](https://www.npmjs.com/package/@cloudbase/js-sdk) ，前端 SDK

一般 node.js 后端项目，引用 `node-sdk` ，只需使用 `latest` 版本：

```json
{
  "dependencies": {
    "@cloudbase/node-sdk": "latest"
  }
}
```

### 初始：全局安装 tcb

安装 tcb 。

以下指令在任意目录执行皆可，不限在具体的工作目录：

```shell
# 全局安装了 @cloudbase/cli
$ npm i -g @cloudbase/cli

# 确认安装成功
$ tcb -v

# 授权登录，执行此指令，会打开浏览器，以登录管理员权限进行授权
$ tcb login
# 一般在 CI/CD 或无法通过网页授权的环境使用
$ tcb login --apiKeyId xxx --apiKey xxx

# 列出现有环境列表，一定要指定 -r 参数
$ tcb env list -r gz

# -r 参数，关联区域
# gz: ap-guangzhou
# bj: ap-beijing
# sh: ap-shanghai

# 创建项目，会依次给你选项目模板，新建项目名称，该指令没啥用
$ tcb new
# tcb new [options] [appName] [templateUri]
# Options:
#   -e, --envId <envId>  环境 Id
#   --clone              从 Git 创建模板时是否 Clone 整个仓库
#   -h, --help           输出帮助信息
```

### 项目环境初始化

#### 项目结构

CloudBase 是可以发布多个云函数，并可选的将若干个云函数映射到路径上，以被外部访问。

> 云函数被外部调用方式主要有两种
> 
> - `app.callFunction({ name: '云函数名', data: { ... } })`
> - 将云函数映射到 path 上，变成一个 URL API  
> 
> 具体请参考腾讯云官方文档，这里不做细节展开。

所以，区别于一个传统的 node.js 项目，我们习惯的项目结构是：

```
node_project
├── src
│   ├── module-a.js
│   ├── module-b.js
│   └── index.js
└── package.json
```

我们习惯于 `index.js` 去调用 `module-a.js` 或 `module-a.js` 的通用逻辑。

而一个 CloudBase 项目的结构，大体会像下面的情况：

```
cloudbase_project
├── functions
│   ├── fn-module-a
│   │   ├── index.js
│   │   └── package.json
│   ├── fn-module-b
│   │   ├── index.js
│   │   └── package.json
│   └── api-index
│       ├── index.js
│       └── package.json
├── cloudbaserc.json
└── package.json
```

和 node.js 项目所不同的时，当我们考虑 `module-a`、`module-b` 可能会被重复调用（通用逻辑）时，，就应该将他们做成是可独立发布的云函数形态。

在这里，我们假定 `api-index` 会被挂载 `/index` 路径上。当我们需要在 `api-index` 中调用 `module-a` 或 `module-b` 时，应该使用 `app.callFunction({ name: 'module-a' })` 。

在 node.js 模式下，所有依赖包都集中在项目根目录的 `package.json` 上。

而 CloudBase 云函数的模式，每个子项目自行管理所需依赖包（具体到发布时，可以自行安装依赖）。

> 基于 `pnpm` [workspace](https://pnpm.io/pnpm-workspace_yaml) 方式进行管理，会是一个更利于本地调试/管理的方式。
> 
> 子项目的 package.json 在 pnpm 环境内，会被项目根目录下的 package.json 统一管理。
> 
> 单独发布子项目时，也只会安装这个子项目所必要的依赖包，而不会管全项目的依赖包。

基于 CloudBase 的这种项目管理模式，更利于将业务拆分，独立测试、发布更新。

#### `cloudbaserc.json` 配置参考

`cloudbaserc.json` 为 CloudBase 的部署配置文件（配置文件字段说明，[参考](https://docs.cloudbase.net/cli-v1/functions/configs#%E5%87%BD%E6%95%B0%E9%85%8D%E7%BD%AE%E9%A1%B9) ），内容格式如下：

```json5
{
  // CloudBase env 环境 ID
  "envId": "endId",
  // 分区，最好指定
  "region": "ap-guangzhou",
  "functionRoot": "./functions",
  "functions": [
    {
      "name": "fn-example",
      // 该云函数的运行超时时间
      "timeout": 5,
      // 环境变量
      "envVariables": {},
      // 运行时的 node 版本号
      "runtime": "Nodejs16.13",
      // 运行时内存限制大小
      "memorySize": 256,
      "handler": "index.main",
      // 部署时同时安装依赖
      "installDependency": true,
      "ignore": ["node_modules", "node_modules/**/*", "*.md", "*.ts"]
    }
  ]
}
```
